<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/*Route::get('/', function()
{
	return View::make('hello');
});*/
/*
Route::get('/env', function() {
    return App::environment();
});*/

Route::get('/', 'IndexController@getIndex');
//Route::controller('blog', 'BlogController');
/*Route::get('blog', function () {
    return Redirect::to('blog/index');
});
Route::get('admin',function(){
    return View::make('admin');
});
Route::resource('blog', 'BlogController');*/
Route::controller('users', 'UsersController');