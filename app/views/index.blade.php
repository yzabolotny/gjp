@extends('layout')
@section('content')
<script type="text/javascript">
                  var logged = false;
                  $(document).ready(function(){
                    $.get( "users/check", function( data ) {
                        logged = data;
                    });
                  });
                  function initialize() {
                    var mapOptions = {
                      //center: { lat: 31.268892266124553, lng: 48.76803719582003},
                      center: new google.maps.LatLng(47, 35),
                      zoom: 5
                    };
                    var map = new google.maps.Map(document.getElementById('map-canvas'),
                        mapOptions);
                        google.maps.event.addListener(map, 'click', function(event) {
                               if(logged){
                                marker = new google.maps.Marker({
                                    position: event.latLng,
                                     map: map,
                                     title: event.latLng.lat().toString() + ' ' + event.latLng.lng().toString(),
                                     draggable: true

                                    });
                                }else{
                                alert( "Please login before put the point" );
                                }

                        });
                  }
                  google.maps.event.addDomListener(window, 'load', initialize);

</script>
<div id="map-canvas"></div>
@stop