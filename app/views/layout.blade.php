<!DOCTYPE html>
<html>
    <head>
        <meta content="text/html; charset=utf-8" http-equiv="content-type">
        <title>@yield('title')</title>
        <meta name="keywords" content="blog,posts,blogs,">
        <meta name="description" content="Blog posts">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>
        {{ HTML::style('css/bootstrap.css') }}
        {{ HTML::style('css/style.css') }}
        {{ HTML::script('') }}
        <style type="text/css">
              html, body, #map-canvas { height: 400px; margin: 0; padding: 0;}
              #map-canvas{margin-top: 50px; }
            </style>
        <script type="text/javascript"
              src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAwq_ZuDAa5YbWrttGgrnKU-n9EW_98od8">
            </script>
        @yield('headExtra')
    </head>
    <body>
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    {{--<li><a class="navbar-link" href="{{action('BlogController@getAdd')}}">New post</a></li>--}}
                    @if (!Auth::check())
                    <li><a class="navbar-link" href="{{action('UsersController@getSignin')}}">Sign in</a></li>
                    <li><a class="navbar-link" href="{{action('UsersController@getSignup')}}">Sign up</a></li>
                    @else
                    <li><a class="navbar-link" href="{{action('UsersController@getSignout')}}">Sign out</a></li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
    @yield('content')
    <div id="footer" class="panel-footer">
        <div class="container">
            <div class="col-md-4">
                &nbsp;
            </div>
        </div>
    </div>
    </body>
</html>