@extends('layout')
@section('content')
<div class="container">
<div id="signupbox" style="margin-top:50px"
     class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
     <div class="panel panel-info">
     <div class="panel-heading">
                     <div class="panel-title">Sign In</div>
                     <!--<div style="float:right; font-size: 80%; position: relative; top:-10px"><a href="#">Forgot password?</a></div>-->
                 </div>
     <div class="panel-body">
{{ Form::open(array('url'=>action('UsersController@postCreate'), 'class'=>'form-signup')) }}
    <h2 class="form-signup-heading">Please Register</h2>

    <div <?php echo ($errors->all())?'style="display:block"' : 'style="display:none"' ?> class="alert alert-danger">
                    <p>Error:</p>
                    @foreach($errors->all() as $error)
                                <span>{{ $error }}</span><br/>
                    @endforeach
    </div>

    </ul>
<div style="margin-bottom: 25px" class="input-group row">
                <label for="firstname" class="col-md-4 control-label">First Name</label>
                <div class="col-md-8">
    {{ Form::text('firstname', null, array('class'=>'form-control input-block-level', 'placeholder'=>'First Name')) }}
    </div>
    <label for="lastname" class="col-md-4 control-label">Last Name</label>
                    <div class="col-md-8">
    {{ Form::text('lastname', null, array('class'=>'form-control input-block-level', 'placeholder'=>'Last Name')) }}
    </div>
    <label for="email" class="col-md-4 control-label">Email Address</label>
                    <div class="col-md-8">
    {{ Form::text('email', null, array('class'=>'form-control input-block-level', 'placeholder'=>'Email Address')) }}
    </div>
    <label for="password" class="col-md-4 control-label">Password</label>
                    <div class="col-md-8">
    {{ Form::password('password', array('class'=>'form-control input-block-level', 'placeholder'=>'Password')) }}
    </div>
    <label for="password_confirmation" class="col-md-4 control-label">Confirm Password</label>
                    <div class="col-md-8">
    {{ Form::password('password_confirmation', array('class'=>'form-control input-block-level', 'placeholder'=>'Confirm Password')) }}
    </div>
    <div style="margin-bottom: 25px" class="form-group row">
                    <!-- Button -->
                    <div class="col-md-offset-3 col-md-9">
                        {{ Form::submit('Register', array('class'=>'btn btn-info icon-hand-right'))}}
                    </div>
                </div>
{{ Form::close() }}
</div>
</div>
</div>
</div>
</div>
@stop