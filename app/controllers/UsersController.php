<?php

class UsersController extends BaseController{
    protected $layout = "layout";
    public function getSignup(){
        return View::make('user.signup');
    }
    public function postCreate(){
        $validator = Validator::make(Input::all(), User::$rules);

        if ($validator->passes()) {
            $user = new User;
            $user->firstname = Input::get('firstname');
            $user->lastname = Input::get('lastname');
            $user->email = Input::get('email');
            $user->password = Hash::make(Input::get('password'));
            $user->save();

            return Redirect::to(action('IndexController@getIndex'))->with('message', 'Thanks for registering!');
        } else {
            return Redirect::to(action('UsersController@getSignup'))->with('message', 'The following errors occurred')->withErrors($validator)->withInput();
        }
    }
    public function postSignin(){
        if (Auth::attempt(array('email'=>Input::get('email'), 'password'=>Input::get('password')))) {
            return Redirect::to('/')->with('message', 'You are now logged in!');
        } else {
            return Redirect::to('users/singin')
                ->with('message', 'Your username/password combination was incorrect')
                ->withInput();
        }
    }
    public function getSignin(){
        return View::make('user.signin');
    }
    public function getSignout(){
        Auth::logout();
        return Redirect::to('/')->with('message', 'Your are now logged out!');
    }
    public function getCheck(){
        if(Auth::check()){
            return Response::json(1);
        }else{
            return Response::json(0);
        }
    }
}